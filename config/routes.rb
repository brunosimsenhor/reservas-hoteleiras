Rails.application.routes.draw do
  root 'home#index'

  resources :clientes
  resources :quartos
  resources :funcionarios
  resources :reservas

  scope :reservas do
    post '/disponibilidade' => 'reservas#disponibilidade', as: :disponibilidade
  end

  get '/login' => 'sessions#new', as: :login
  post '/login' => 'sessions#create'
  get '/logout' => 'sessions#destroy', as: :logout

  get '/relatorios/dashboard' => 'relatorios#dashboard', as: :dashboard
end
