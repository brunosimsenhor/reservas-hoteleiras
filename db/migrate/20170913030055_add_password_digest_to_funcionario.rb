class AddPasswordDigestToFuncionario < ActiveRecord::Migration
  def up
    add_column :funcionarios, :password_digest, :string
  end

  def down
    remove_column :funcionarios, :password_digest
  end
end
