class AddDeletedAtToQuartos < ActiveRecord::Migration
  def up
  	add_column :quartos, :deleted_at, :datetime
  end

  def down
  	remove_column :quartos, :deleted_at
  end
end
