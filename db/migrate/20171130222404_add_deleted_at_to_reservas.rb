class AddDeletedAtToReservas < ActiveRecord::Migration
  def up
  	add_column :reservas, :deleted_at, :datetime
  end

  def down
  	remove_column :reservas, :deleted_at
  end
end
