class CreateClientes < ActiveRecord::Migration
  def up
    create_table :clientes, id: false do |t|
      t.primary_key :idcliente, :primary_key, null: false
      t.string :nome, null: false
      t.string :email
      t.string :rg
      t.string :cpf
      t.string :telefone
      t.string :celular

      t.timestamps
    end
  end

  def down
    drop_table :clientes
  end
end
