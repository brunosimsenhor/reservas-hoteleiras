class AddDataNascimentoToCliente < ActiveRecord::Migration
  def up
    add_column :clientes, :data_nascimento, :date, null: true
  end

  def down
    remove_column :clientes, :data_nascimento
  end
end
