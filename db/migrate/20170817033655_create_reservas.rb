class CreateReservas < ActiveRecord::Migration
  def up
    create_table :reservas, id: false do |t|
      t.primary_key :idreserva, :primary_key, null: false

      t.integer :idfuncionario, null: false
      t.integer :idquarto, null: false
      t.integer :idcliente, null: false

      t.date :data_entrada, null: false
      t.date :data_saida, null: false

      t.boolean :cafe_manha, null: false
      t.boolean :almoco, null: false
      t.boolean :jantar, null: false

      t.float :valor_total, precision: 10, scale: 2, null: false

      t.text :observacoes

      t.timestamps null: false
    end
  end

  def down
    drop_table :reservas
  end
end
