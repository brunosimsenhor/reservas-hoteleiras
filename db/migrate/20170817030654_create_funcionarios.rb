class CreateFuncionarios < ActiveRecord::Migration
  def change
    create_table :funcionarios, id: false do |t|
      t.primary_key :idfuncionario, :primary_key, null: false
      t.string :nome, null: false
      t.string :email
      t.string :rg
      t.string :cpf, null: false
      t.string :telefone
      t.string :celular
      t.date :data_nascimento, null: true

      t.timestamps
      t.datetime :deleted_at
    end
  end
end
