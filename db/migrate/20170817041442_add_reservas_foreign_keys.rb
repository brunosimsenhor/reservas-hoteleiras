class AddReservasForeignKeys < ActiveRecord::Migration
  def up
    add_foreign_key :reservas, :funcionarios, column: :idfuncionario, primary_key: :idfuncionario
    add_foreign_key :reservas, :quartos, column: :idquarto, primary_key: :idquarto
    add_foreign_key :reservas, :clientes, column: :idcliente, primary_key: :idcliente
  end

  def down
    remove_foreign_key :reservas, column: :idfuncionario
    remove_foreign_key :reservas, column: :idquarto
    remove_foreign_key :reservas, column: :idcliente
  end
end
