class AddUltimoLoginToFuncionario < ActiveRecord::Migration
  def up
    add_column :funcionarios, :ultimo_login, :datetime
  end

  def down
    remove_column :funcionarios, :ultimo_login
  end
end
