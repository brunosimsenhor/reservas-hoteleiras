class CreateQuartos < ActiveRecord::Migration
  def up
    create_table :quartos, id: false do |t|
      t.primary_key :idquarto, :primary_key, null: false
      t.integer :numero, null: false
      t.decimal :valor_diaria, null: false, precision: 10, scale: 2
      t.integer :numero_pessoas, null: false

      t.timestamps null: false
    end
  end

  def down
    drop_table :quartos
  end
end
