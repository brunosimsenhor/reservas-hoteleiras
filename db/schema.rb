# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171130222404) do

  create_table "clientes", primary_key: "idcliente", force: :cascade do |t|
    t.string   "nome",            limit: 255, null: false
    t.string   "email",           limit: 255
    t.string   "rg",              limit: 255
    t.string   "cpf",             limit: 255
    t.string   "telefone",        limit: 255
    t.string   "celular",         limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "data_nascimento"
  end

  create_table "funcionarios", primary_key: "idfuncionario", force: :cascade do |t|
    t.string   "nome",            limit: 255, null: false
    t.string   "email",           limit: 255
    t.string   "rg",              limit: 255
    t.string   "cpf",             limit: 255, null: false
    t.string   "telefone",        limit: 255
    t.string   "celular",         limit: 255
    t.date     "data_nascimento"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.string   "password_digest", limit: 255
    t.datetime "ultimo_login"
  end

  create_table "quartos", primary_key: "idquarto", force: :cascade do |t|
    t.integer  "numero",         limit: 4,                          null: false
    t.decimal  "valor_diaria",             precision: 10, scale: 2, null: false
    t.integer  "numero_pessoas", limit: 4,                          null: false
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.datetime "deleted_at"
  end

  create_table "reservas", primary_key: "idreserva", force: :cascade do |t|
    t.integer  "idfuncionario", limit: 4,     null: false
    t.integer  "idquarto",      limit: 4,     null: false
    t.integer  "idcliente",     limit: 4,     null: false
    t.date     "data_entrada",                null: false
    t.date     "data_saida",                  null: false
    t.boolean  "cafe_manha",                  null: false
    t.boolean  "almoco",                      null: false
    t.boolean  "jantar",                      null: false
    t.float    "valor_total",   limit: 24,    null: false
    t.text     "observacoes",   limit: 65535
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.datetime "deleted_at"
  end

  add_index "reservas", ["idcliente"], name: "fk_rails_f982826b97", using: :btree
  add_index "reservas", ["idfuncionario"], name: "fk_rails_fb5fa63368", using: :btree
  add_index "reservas", ["idquarto"], name: "fk_rails_42db745d0f", using: :btree

  add_foreign_key "reservas", "clientes", column: "idcliente", primary_key: "idcliente"
  add_foreign_key "reservas", "funcionarios", column: "idfuncionario", primary_key: "idfuncionario"
  add_foreign_key "reservas", "quartos", column: "idquarto", primary_key: "idquarto"
end
