Util.mark 'Seed'

duration = Benchmark.realtime {
  # Clientes
  numero_clientes = Faker::Number.between(40, 100)

  Util.action "Criando #{numero_clientes} clientes"

  numero_clientes.times do |i|
    Cliente.create!(
      nome: Faker::Name.name,
      email: Faker::Internet.email,
      rg: Faker::Number.number(10),
      cpf: Faker::CPF.numeric.to_s,
      data_nascimento: Faker::Date.birthday(18, 65),
      telefone: Util.mask(Faker::Number.number(11), Util::Mask::TELEFONE),
      celular: Util.mask(Faker::Number.number(11), Util::Mask::TELEFONE)
    )

    percentual = ((i + 1) / numero_clientes.to_f) * 100
    Util.sub_action "#{percentual.round}\%", true

    $stdout.flush
  end

  puts "\n"

  # Quartos
  numero_andares = 6
  numero_quartos_por_andar = 8
  numero_total_quartos = numero_andares * numero_quartos_por_andar

  Util.action "Criando #{numero_total_quartos} quartos"

  (1..numero_andares).each do |andar|
    (1..numero_quartos_por_andar).each do |numero|
      Quarto.create!(
        numero: (andar * 100) + numero,
        numero_pessoas: (numero / 2.0).ceil,
        valor_diaria: 10 + (andar * 10) + (numero * 5)
      )

      percentual = ((andar * numero) / numero_total_quartos.to_f) * 100
      Util.sub_action "#{percentual.round}\%", true

      $stdout.flush
    end
  end

  puts "\n"

  # Funcionários
  Util.action "Criando funcionário Administrador"

  funcionario_master = Funcionario.find_by(idfuncionario: 1) || Funcionario.new(id: 1)
  funcionario_master.nome = 'Administrador'
  funcionario_master.cpf = '00000000000'
  funcionario_master.email = nil
  funcionario_master.rg = nil
  funcionario_master.data_nascimento = nil
  funcionario_master.telefone = nil
  funcionario_master.celular = nil
  funcionario_master.save!

  numero_funcionarios = Faker::Number.between(6, 15)

  Util.action "Criando #{numero_funcionarios} funcionários"

  numero_funcionarios.times do |i|
    Funcionario.create!(
      nome: Faker::Name.name,
      email: Faker::Internet.email,
      rg: Faker::Number.number(10),
      cpf: Faker::CPF.numeric.to_s,
      data_nascimento: Faker::Date.birthday(18, 65),
      telefone: Util.mask(Faker::Number.number(11), Util::Mask::TELEFONE),
      celular: Util.mask(Faker::Number.number(11), Util::Mask::TELEFONE)
    )

    percentual = ((i + 1) / numero_funcionarios.to_f) * 100
    Util.sub_action "#{percentual.round}\%", true

    $stdout.flush
  end

  puts "\n"
}

Util.mark 'Seed', duration