# == Schema Information
#
# Table name: reservas
#
#  idreserva     :integer          not null, primary key
#  idfuncionario :integer          not null
#  idquarto      :integer          not null
#  idcliente     :integer          not null
#  data_entrada  :date             not null
#  data_saida    :date             not null
#  cafe_manha    :boolean          not null
#  almoco        :boolean          not null
#  jantar        :boolean          not null
#  valor_total   :float(24)        not null
#  observacoes   :text(65535)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Reserva < ActiveRecord::Base
  include ApplicationRecord

  # Campos de busca
  SEARCH_FIELDS = [
    :idfuncionario,
    :idquarto,
    :idcliente
  ]

  # Soft delete: indica para que o registro não seja excluído físicamente
  # do banco de dados, apenas salva a data da remoção e automaticamente
  # não retorna os itens já removidos do sistema
  acts_as_paranoid

  # Define a associação entre reserva e funcionário.
  belongs_to :funcionario, -> { with_deleted }, foreign_key: :idfuncionario

  # Define a associação entre reserva e quarto.
  belongs_to :quarto, foreign_key: :idquarto

  # Define a associação entre reserva e cliente.
  belongs_to :cliente, foreign_key: :idcliente

  # Valida a obrigatoriedade do funcionário, do quarto e do cliente.
  validates :funcionario, :quarto, :cliente, presence: true

  # Valida a obrigatoriedade da data de entrada, da data de saída e do valor total.
  validates :data_entrada, :data_saida, :valor_total, presence: true

  # Valida se os dados das refeições foram inseridos corretamente.
  validates :cafe_manha, :almoco, :jantar, inclusion: [true, false]

  # Executa uma validação personalizada.
  validate :valida_data_reserva

  # Antes da validação, define alguns valores para o registro.
  before_validation :set_default_values

  # Formata o número da reserva para o padrão "00001"
  def numero_reserva
    idreserva.to_s.rjust(5, '0') if idreserva?
  end

  # Preenche automaticamente o valor total baseado no valor da diária
  def set_default_values
    return true unless self.quarto.present? && self.data_entrada.present? && self.data_saida.present? && self.data_entrada < self.data_saida

    total_dias = (self.data_saida - self.data_entrada).to_i

    self.valor_total = total_dias * self.quarto.valor_diaria.to_f
  end

  # Valida se a data de entrada é menor que a data de saída.
  def valida_entrada_saida
    unless self.data_entrada < self.data_saida
      errors.add(:data_entrada, 'A data de entrada precisa ser menor do que a de saída.')
      return false
    end
  end

  # Valida se a data de saída é maior que a data de entrada.
  def valida_data_reserva
    return true unless self.quarto.present? && self.data_entrada? && self.data_saida?

    reserva = Reserva.where(idquarto: self.idquarto).where("data_entrada < ? AND data_saida > ?", data_saida, data_entrada).where.not(idreserva: self.idreserva).first

    if reserva.present?
      errors.add(:base, "Já existe uma reserva para o quarto #{reserva.quarto.numero} na data #{reserva.data_entrada.strftime('%d/%m/%Y')} à #{reserva.data_saida.strftime('%d/%m/%Y')}.")
      return false
    end
  end

  # Monta um conjunto com as refeições definidas na reserva.
  def refeicoes
    arr = []

    arr << 'Café da manhã' if self.cafe_manha?
    arr << 'Almoço' if self.almoco?
    arr << 'Jantar' if self.jantar?

    arr
  end

  # Recupera as informações das reservas e as formata para o padrão da
  # estrutura do dashboard.
  def self.dashboard_info data_inicial, data_final, only_occupied = nil
    info = {}

    reservas = Reserva.where("data_entrada < ? AND data_saida > ?", data_final, data_inicial)

    if only_occupied.present?
      reservas = reservas.limit(only_occupied)
    end

    reservas.each do |reserva|
      info[reserva.idquarto] ||= []

      (reserva.data_entrada..(reserva.data_saida - 1.day)).each do |tmp_data|
        info[reserva.idquarto].push tmp_data
      end
    end

    info
  end
end
