# == Schema Information
#
# Table name: clientes
#
#  idcliente       :integer          not null, primary key
#  nome            :string(255)      not null
#  email           :string(255)      not null
#  rg              :string(255)      not null
#  cpf             :string(255)      not null
#  telefone        :string(255)
#  celular         :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  data_nascimento :date
#

class Cliente < ActiveRecord::Base
  include ApplicationRecord

  # Campos de busca
  SEARCH_FIELDS = [
    :nome,
    :email
  ]

  # Valida a obrigatoriedade do nome.
  validates :nome, presence: true

  # Valida a obrigatoriedade e o formato do CPF.
  validates :cpf, cpf: true

  # Valida a unicidade do e-mail e do CPF.
  validates :email, :cpf, uniqueness: true

  # Antes da validação, formata alguns valores padrão.
  before_validation :set_default_values

  # Impede a remoção de clientes.
  before_destroy :validate_destroy

  # Adiciona um erro e não permite a remoção do cliente.
  def validate_destroy
    errors.add(:base, 'Clientes não podem ser removidos.')
    return false
  end

  # Remove a pontuação do CPF antes de validá-lo.
  def set_default_values
    self.cpf = self.cpf.gsub(/[^0-9]/, '') if self.cpf.present?
  end
end
