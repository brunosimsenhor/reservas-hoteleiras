# == Schema Information
#
# Table name: quartos
#
#  idquarto       :integer          not null, primary key
#  numero         :integer          not null
#  valor_diaria   :decimal(10, 2)   not null
#  numero_pessoas :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Quarto < ActiveRecord::Base
  include ApplicationRecord

  # Campos de busca
  SEARCH_FIELDS = [
    :numero
  ]

  # Soft delete: indica para que o registro não seja excluído físicamente
  # do banco de dados, apenas salva a data da remoção e automaticamente
  # não retorna os itens já removidos do sistema
  acts_as_paranoid

  # Define o relacionamento (n:m) entre quartos e reservas.
  has_many :reservas, foreign_key: :idquarto

  # Valida a obrigatoriedade do número do quarto, do valor da diária e do número de pessoas.
  validates :numero, :valor_diaria, :numero_pessoas, presence: true

  # Valida a unicidade do número do quarto.
  validates :numero, uniqueness: true
end
