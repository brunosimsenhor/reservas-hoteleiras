# == Schema Information
#
# Table name: funcionarios
#
#  idfuncionario   :integer          not null, primary key
#  nome            :string(255)      not null
#  email           :string(255)      not null
#  rg              :string(255)      not null
#  cpf             :string(255)      not null
#  telefone        :string(255)
#  celular         :string(255)
#  data_nascimento :date
#  created_at      :datetime
#  updated_at      :datetime
#  deleted_at      :datetime
#

class Funcionario < ActiveRecord::Base
  include ApplicationRecord

  # idfuncionario do usuário administrador.
  ADMIN = 1

  # Campos de busca
  SEARCH_FIELDS = [
    :nome,
    :email
  ]

  # Define o relacionamento (n:m) entre funcionários e reservas.
  has_many :reservas, foreign_key: :idquarto

  # Soft delete: indica para que o registro não seja excluído físicamente
  # do banco de dados, apenas salva a data da remoção e automaticamente
  # não retorna os itens já removidos do sistema
  acts_as_paranoid

  # Adiciona os métodos de validação da senha.
  has_secure_password

  # Valida a obrigatoriedade do nome e do CPF.
  validates :nome, :cpf, presence: true

  # Valida o formato do CPF, a não ser que seja o registro do usuário administrador.
  validates :cpf, cpf: true, unless: :is_admin_user?

  # Valida a unicidade do CPF.
  validates :cpf, uniqueness: true

  # Valida a unicidade do e-mail, se o e-mail estiver cadastrado.
  validates :email, uniqueness: true, if: :email?

  # Antes da validação, define alguns valores para o registro.
  before_validation :set_default_values

  def set_default_values
    # Remove a pontuação do CPF antes de validá-lo.
    self.cpf = self.cpf.gsub(/[^0-9]/, '') if self.cpf.present?

    # Aplica uma criptografia na senha.
    self.password_digest = BCrypt::Password.create(self.cpf)
  end

  # Retorna se o funcionário em questão é o administrador do sistema.
  def is_admin_user?
    id == ADMIN
  end
end
