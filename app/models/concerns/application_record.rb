module ApplicationRecord
  extend ActiveSupport::Concern

  SEARCH_FIELDS = []

  included do |base|
    base.primary_key = "id#{base.name.downcase}"
  end

  module ClassMethods
    def humanize_search_fields
      arr = []
      
      self::SEARCH_FIELDS.each do |field|
        arr.push self.human_attribute_name field
      end

      arr
    end
  end
end