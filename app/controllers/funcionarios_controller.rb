class FuncionariosController < ApplicationController
  # Antes de qualquer ação nesse controller, executa o método "is_admin_user?".
  before_action :is_admin_user?

  # Títulos utilizados nas telas.
  TITLES = {
    index: 'Listagem de funcionários',
    create: 'Novo funcionário',
    show: 'Visualizar funcionário',
    update: 'Editar funcionário',
    destroy: 'Remover funcionário'
  }

  # Verifica se o usuário está logado e se ele é o usuário administrador.
  def is_admin_user?
    unless current_user.is_admin_user?
      flash[:error] = "Você não tem permissão para acessar essa funcionalidade."
      redirect_to root_url
    end
  end

  # Definindo qual é a model usada pela controller.
  def model_klass
    Funcionario
  end

  # Aplicando a ordenação.
  def apply_sorters
    @items = @items.order(:nome)
  end

  # Permite somente parâmetros através de uma "white list".
  def model_params
    params.require(:funcionario).permit(:nome, :email, :rg, :cpf, :telefone, :celular, :data_nascimento)
  end
end
