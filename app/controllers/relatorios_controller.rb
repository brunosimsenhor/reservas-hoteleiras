class RelatoriosController < ApplicationController
  # GET /relatorios/dashboard
  def dashboard
    # Prepara os parâmetros para montar os dados da tela de Dashboard. 
    @data = if params.key?(:data) && params[:data].present?
      Date.parse(params[:data])
    else
      Date.today
    end

    @data_inicial = @data - 7.days
    @data_final = @data + 7.days
    @only_occupied = false

    # Chamamos o método que recupera as informações formatadas das reservas.
    @info = Reserva.dashboard_info @data_inicial, @data_final
  end
end
