class ReservasController < ApplicationController
  skip_before_filter :verify_authenticity_token, :only => [:disponibilidade]

  # Títulos utilizados nas telas.
  TITLES = {
    index: 'Listagem de reservas',
    create: 'Nova reserva',
    show: 'Visualizar reserva',
    update: 'Editar reserva',
    destroy: 'Remover reserva'
  }

  # Definindo qual é a model usada pela controller.
  def model_klass
    Reserva
  end

  # Permite somente parâmetros através de uma "white list".
  def model_params
    params.require(:reserva).permit(:idfuncionario, :idquarto, :idcliente, :data_entrada, :data_saida, :cafe_manha, :almoco, :jantar, :observacoes)
  end

  def disponibilidade
    id = params[:id].to_i
    data_entrada = Date.parse(params[:data_entrada])
    data_saida = Date.parse(params[:data_saida])

    query = Reserva.select(:idquarto).where("data_entrada < ? AND data_saida > ?", data_saida, data_entrada)

    unless id.zero?
      query = query.where.not(idreserva: id)
    end

    quartos = Quarto.where("idquarto NOT IN(#{query.to_sql})")

    dias_reserva = (data_saida - data_entrada).to_i

    result = quartos.collect do |quarto|
      {
        idquarto: quarto.idquarto,
        numero: quarto.numero,
        valor_reserva: quarto.valor_diaria.to_f * dias_reserva
      }
    end

    render json: { data: result, meta: { success: true } }
  end
end
