class ClientesController < ApplicationController
  # Títulos utilizados nas telas.
  TITLES = {
    index: 'Listagem de clientes',
    create: 'Novo cliente',
    show: 'Visualizar cliente',
    update: 'Editar cliente',
    destroy: 'Remover cliente'
  }

  # Definindo qual é a model usada pela controller.
  def model_klass
    Cliente
  end

  # Aplicando a ordenação.
  def apply_sorters
    @items = @items.order(:nome)
  end

  # Permite somente parâmetros através de uma "white list".
  def model_params
    params.require(:cliente).permit(:nome, :email, :rg, :cpf, :telefone, :celular, :data_nascimento)
  end
end
