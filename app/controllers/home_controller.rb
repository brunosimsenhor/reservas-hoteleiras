class HomeController < ApplicationController
  def index
    # Montamos as informações para listar as reservas dos próximos três dias.
    @data = Date.today

    @data_inicial = @data
    @data_final = @data + 3.days
    @only_occupied = 10

    # Chamamos o método que recupera as informações formatadas das reservas.
    @info = Reserva.dashboard_info @data_inicial, @data_final, @only_occupied
  end
end
