class QuartosController < ApplicationController
  # Títulos utilizados nas telas.
  TITLES = {
    index: 'Listagem de quartos',
    create: 'Novo quarto',
    show: 'Visualizar quarto',
    update: 'Editar quarto',
    destroy: 'Remover quarto'
  }

  # Definindo qual é a model usada pela controller.
  def model_klass
    Quarto
  end

  # Aplicando a ordenação.
  def apply_sorters
    @items = @items.order(:numero)
  end

  # Permite somente parâmetros através de uma "white list".
  def model_params
    params.require(:quarto).permit(:numero, :valor_diaria, :numero_pessoas)
  end
end
