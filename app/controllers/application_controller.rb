class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :authorize

  TITLES = {
    index: 'Listagem de registros',
    create: 'Novo registro',
    show: 'Visualizar registro',
    update: 'Alterar registro',
    destroy: 'Remover registro'
  }

  before_action :set_item, only: [:show, :edit, :update, :destroy]
  before_action :set_title

  def current_user
    @current_user ||= Funcionario.find(session[:idfuncionario]) if session[:idfuncionario].present? && Funcionario.exists?(session[:idfuncionario])
  end

  helper_method :current_user

  def authorize
    redirect_to login_url unless current_user
  end

  # GET /#{items}
  def index
    @items = model_klass.page get_page

    apply_filters
    apply_sorters
  end

  # GET /#{items}/1
  def show
  end

  # GET /#{items}/new
  def new
    @item = self.model_klass.new
  end

  # GET /#{items}/1/edit
  def edit
  end

  # POST /#{items}
  def create
    @item = self.model_klass.new(self.model_params)

    if @item.save
      redirect_to @item, notice: "Registro criado com sucesso!"
    else
      render :new
    end
  end

  # PATCH/PUT /#{items}/1
  def update
    if @item.update(self.model_params)
      redirect_to @item, notice: "Registro alterado com sucesso!"
    else
      render :edit
    end
  end

  # DELETE /#{items}/1
  def destroy
    url = :"#{self.controller_name.pluralize}_url"

    if @item.destroy
      flash[:notice] = "Registro removido com sucesso!"

    else
      flash[:error] = "Atenção! #{@item.errors.full_messages.to_sentence}"
    end

    redirect_to self.try(url)
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_item
    @item = self.model_klass.find(params[:id])
  end

  def set_title
    @all_titles = self.class::TITLES

    # Tratando títulos.
    action = case self.action_name.to_sym
    when :new
      :create
    when :edit
      :update
    else
      self.action_name.to_sym
    end

    if self.class::TITLES.present? && self.class::TITLES[action].present?
      @title = self.class::TITLES[action]
    end
  end

  def get_page
    params[:page] || 1
  end

  protected

    def apply_filters
      if self.model_klass::SEARCH_FIELDS.any? && params[:search].present?
        arr = []

        self.model_klass::SEARCH_FIELDS.each do |field|
          association = self.model_klass.reflect_on_all_associations.find { |a| a.foreign_key == field }

          if association.present?
            @items = @items.joins(association.name) unless @items.joins_values.include?(association.name)

            if association.klass::SEARCH_FIELDS.any?
              association.klass::SEARCH_FIELDS.each do |field|
                arr.push self.build_query(association.klass, field, params[:search])
              end
            end

          else
            arr.push self.build_query(self.model_klass, field, params[:search])
          end
        end
      
        @items = @items.where(arr.join(' OR '))
      end
    end

    # Sobrescrever nas classes descendentes.
    def apply_sorters
      
    end
  
    def build_query model_klass, field_name, search_query
      column_info = model_klass.columns_hash[field_name.to_s]
      
      raise 'Coluna \'%s\' não encontrada' % field_name unless column_info.present?

      case column_info.type
      when :integer, :decimal, :float
        model_klass.arel_table[field_name].eq(search_query).to_sql

      else
        model_klass.arel_table[field_name].matches("%#{search_query}%").to_sql
      end
    end

    # Only allow a trusted parameter "white list" through.
    def model_params
      raise "Precisa definir o método 'model_params'"
    end

    def model_klass
      raise "Precisa definir o método 'model_klass'"
    end
end
