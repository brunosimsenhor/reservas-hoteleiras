class SessionsController < ActionController::Base
  # Tela de login
  # Define algumas informações para montar a tela corretamente.
  def new
    @body_css_class = 'login'

    render layout: 'application'
  end

  # Login
  # Executa o login.
  def create
    funcionario = Funcionario.find_by_cpf(params[:code])

    # Se encontrar o funcionário e conseguir autenticá-lo, prosseguir.
    if funcionario.present? && funcionario.authenticate(params[:code])
      ultimo_login = funcionario.ultimo_login

      # Mensagem de boas-vindas.
      flash[:info] = "Bem-vindo, sr(a). #{funcionario.nome}, " + if ultimo_login.present?
        "seu último login foi em #{ultimo_login.strftime('%d/%m/%Y %H:%M:%S')}."
      else
        "este é o seu primeiro login."
      end

      # Atualiza a data de último de login do usuário.
      funcionario.update_column(:ultimo_login, DateTime.now)
      funcionario.save

      # Define o usuário na sessão.
      session[:idfuncionario] = funcionario.id

      redirect_to :root

    # Não conseguiu autenticar, mostra uma mensagem de erro e
    # redireciona o usuário para a tela de login.
    else
      flash[:error] = 'Acesso não autorizado!'

      redirect_to login_url
    end
  end

  # Logout
  # Desloga o usuário e o redireciona para a tela de login.
  def destroy
    session[:idfuncionario] = nil

    redirect_to login_url
  end
end
