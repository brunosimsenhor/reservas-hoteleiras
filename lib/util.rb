puts 'Loading Util module...'

module Util
  module Mask
    CPF = '999.999.999-99'
    TELEFONE = '(99) 99999-9999'
  end

  def self.mask string, mask, wildcard = '9'
    count = 0
    masked_string = ''

    mask.split('').each do |char|
      if char == wildcard
        masked_string += string[count] if string[count].present?
        count += 1

      else
        masked_string += char
      end
    end

    masked_string
  end

  def self.mask_telefone string
    mask string, Mask::TELEFONE
  end

  def self.mask_cpf string
    mask string, Mask::CPF
  end

  def self.mark _str, duration=nil, len=80
    str = _str.to_s
    str += " (#{duration.round(4).to_s}s)" if duration.present?
  
    puts "== #{str} #{'=' * [1, len - 5 - str.size].max}"
  end
  
  def self.action str, r_line=false
    if r_line
      print "\r #{str.to_s}"
    else
      puts "-- #{str.to_s}"
    end
  end

  def self.sub_action str, r_line=false
    if r_line
      print "\r   -> #{str.to_s}"
    else
      puts "   -> #{str.to_s}"
    end
  end

  def self.duration duration, r_line=false
    sub_action "#{duration.round(4).to_s}s", r_line
  end
end